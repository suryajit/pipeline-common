# Nightly

This section contains job definitions for [nightly](https://gitlab.com/gitlab-org/quality/nightly) test pipelines

## Job definitions

### qa.gitlab-ci.yml

QA test job definitions

### reports.gitlab-ci.yml

Reporting job definition:

* allure test report
* knapsack report for parallel runs
* slack notification
